package com.example.admintor.trafficsandbox;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;

import java.util.List;


/**
 * Created by SunLight on 2017/6/16.
 */

public class MyAlerDialog extends AlertDialog implements AdapterView.OnItemClickListener {
    ListView listView;
    ProgressBar progressBar;
    MyBaseadapter baseAdapter;
    List<String[]> list;
    private OnEditInputFinishedListener mListener;
    Context context;
    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (msg.what == 0x123) {
                baseAdapter = new MyBaseadapter(context);
                progressBar.setVisibility(View.GONE);
                listView.setVisibility(View.VISIBLE);
                baseAdapter.setData(list);
                listView.setAdapter(baseAdapter);
            }
        }
    };

    public MyAlerDialog(Context context, List<String[]> list, OnEditInputFinishedListener mListener) {
        super(context);
        this.context = context;
        this.list = list;
        this.mListener = mListener;
    }

    public void setData(List<String[]> list) {
        this.list = list;
    }

    public interface OnEditInputFinishedListener {
        void editInputFinished(String ips, String serials, int number);
    }

    public void refurbish() {
        handler.sendEmptyMessage(0x123);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mylistview);
        getWindow().setLayout(700, 600);
        progressBar = (ProgressBar) findViewById(R.id.bar);
        listView = (ListView) findViewById(R.id.list);
        listView.setOnItemClickListener(this);

    }

    @Override
    protected void onStart() {
        progressBar.setVisibility(View.VISIBLE);
        listView.setVisibility(View.GONE);
        super.onStart();
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
        if (mListener != null) {

            String ip = list.get(position)[0];
            String serial = list.get(position)[1];
            mListener.editInputFinished(ip, serial,Integer.parseInt(list.get(position)[2]));
            dismiss();
        }


    }
}
