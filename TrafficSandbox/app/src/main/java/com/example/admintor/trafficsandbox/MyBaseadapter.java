package com.example.admintor.trafficsandbox;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;


public class MyBaseadapter extends BaseAdapter {

    Context context;
    List<String[]> list;

    public MyBaseadapter(Context context) {
        this.context = context;
    }

    public void setData(List<String[]> list) {

        if (list != null) {
            this.list = list;
        }

    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {

        return list.get(position);
    }

    @Override
    public long getItemId(int position) {

        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHodler hodler = null;
        if (convertView == null) {
            hodler = new ViewHodler();
            convertView = LayoutInflater.from(context).inflate(R.layout.mybase_layout_item, null);
            hodler.img = (ImageView) convertView.findViewById(R.id.img);
            hodler.ip = (TextView) convertView.findViewById(R.id.ip);
            hodler.port = (TextView) convertView.findViewById(R.id.port);
            hodler.classs = (TextView) convertView.findViewById(R.id.classs);
            hodler.homeip = (TextView) convertView.findViewById(R.id.homeip);
            convertView.setTag(hodler);
        } else {
            hodler = (ViewHodler) convertView.getTag();
        }
        hodler.img.setBackgroundResource(R.drawable.wifi);
        hodler.ip.setText("IP:" + list.get(position)[0]);
        hodler.port.setText("Port:" + 5800);
        hodler.classs.setText("LocalPort:" + list.get(position)[1]);
        hodler.homeip.setText("LocalNum:" + list.get(position)[2]);
        return convertView;
    }

    class ViewHodler {
        private TextView ip;
        private TextView port;
        private ImageView img;
        private TextView classs;
        private TextView homeip;
    }
}
