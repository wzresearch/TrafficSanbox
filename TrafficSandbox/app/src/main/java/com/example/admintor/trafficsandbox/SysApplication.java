package com.example.admintor.trafficsandbox;

import java.util.LinkedList;
import java.util.List;

import android.app.Activity;
import android.app.Application;

public class SysApplication extends Application {
    private List<Activity> activityList=new LinkedList<Activity>();
    private static SysApplication instance;

    private SysApplication() {
    }

    //单例模式中获取唯一的ExitApplication实例
    public static SysApplication getInstance() {
        if (null == instance) {
            instance = new SysApplication();
        }
        return instance;
    }

    //添加Activity到容器中
    public void addActivity(Activity activity) {
        activityList.add(activity);
    }

    //遍历所有Activity并finish
    public void exit() {
        for (Activity activity : activityList) {
            activity.finish();
        }
        System.exit(0);
    }
}