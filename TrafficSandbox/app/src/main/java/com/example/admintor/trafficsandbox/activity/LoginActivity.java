package com.example.admintor.trafficsandbox.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.admintor.trafficsandbox.MyAlerDialog;
import com.example.admintor.trafficsandbox.R;
import com.example.admintor.trafficsandbox.SysApplication;
import com.example.admintor.trafficsandbox.data.Data;
import com.example.admintor.trafficsandbox.data.ReciveDateHander;
import com.example.wzsdk.callback.LoginStateCallback;
import com.example.wzsdk.callback.SearchFinish;
import com.example.wzsdk.login.Landing;
import com.example.wzsdk.login.TargetInfo;
import com.example.wzsdk.scan.Search;
import com.example.wzsdk.tools.StringValidationUtils;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Admintor on 2017/11/7.
 */

public class LoginActivity extends Activity implements MyAlerDialog.OnEditInputFinishedListener, SearchFinish, LoginStateCallback {
    private Search wzoneSearch;
    private Landing wzoneLanding;
    MyAlerDialog myAlerDialog;
    @BindView(R.id.ip)
    EditText ip;
    @BindView(R.id.sn)
    EditText sn;
    @BindView(R.id.login)
    Button login;
    @BindView(R.id.search)
    Button search;
    private ProgressDialog progressDialog;


    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 0x123:
                    handler.sendEmptyMessageDelayed(0x456, 1000);
                    break;
                case 0x456:
                    progressDialog.dismiss();
                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    intent.putExtra("DateHandler", reciveDateHander);
                    startActivity(intent);
                    break;
            }
        }
    };

    ReciveDateHander reciveDateHander;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        initSdk();
        myAlerDialog = new MyAlerDialog(this, list, this);
        SysApplication.getInstance().addActivity(this);
    }

    public void initSdk() {
        reciveDateHander = new ReciveDateHander(this);
        wzoneSearch = reciveDateHander.getWzoneSearch();
        wzoneSearch.setSearchListener(this);
        wzoneLanding = reciveDateHander.getWzoneLanding();
        wzoneLanding.setLoginListener(this);
    }


    @OnClick({R.id.login, R.id.search})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.login:
                String ips = ip.getText().toString().trim();
                if (StringValidationUtils.validateRegex(ips, StringValidationUtils.RegexIP)) {
                    TargetInfo targetInfo = new TargetInfo();
                    targetInfo.setIp(ips);
                    targetInfo.setPort(5800);
                    wzoneLanding.WzoneLogin(targetInfo);
                    progressDialog = ProgressDialog.show(this, "系统提示", "正在加载中...", true, false);
                }
                break;
            case R.id.search:
                myAlerDialog.show();
                wzoneSearch.StartSearchToWifi();
                break;
        }
    }


    List<String[]> list = new ArrayList<>();


    @Override
    public void editInputFinished(String ips, String serials, int number) {
        ip.setText(ips);
        sn.setText(serials);
    }

    @Override
    public void LoginStatus(boolean state, String sn) {
        if (state) {
            handler.sendEmptyMessage(0x123);
        } else {
            Toast.makeText(this, "登录失败", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void AttentionStarting(String uiid, byte b, String index, String s2, String s3) {
        reciveDateHander.ReciverDispose(uiid, b, index);
    }

    @Override
    public void AttentionProcess(String uiid, byte b, String s1, String index, String s3, String s4) {
        reciveDateHander.ReciverDispose(uiid, b, index);
    }

    @Override
    public void SetVarState(String s, byte b, String s1) {

    }

    @Override
    public void SetKeyState(String s, String s1, String s2) {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        wzoneLanding.exitLogin();
    }

    @Override
    public void onSearchFinsh(List<String[]> list) {
        if (list != null) {
            myAlerDialog.setData(list);
            myAlerDialog.refurbish();
        }
    }


}
