package com.example.admintor.trafficsandbox.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.admintor.trafficsandbox.data.Data;
import com.example.admintor.trafficsandbox.R;
import com.example.admintor.trafficsandbox.SysApplication;
import com.example.admintor.trafficsandbox.data.ReciveBean;
import com.example.admintor.trafficsandbox.data.ReciveDateHander;
import com.example.wzsdk.login.Landing;
import com.ipcamera.demo.AddCameraActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {


    Handler atnhandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 0x123:
                    wzoneLanding.AttenVarsByUiid(Data.H_T_UIID);
                    atnhandler.sendEmptyMessageDelayed(0x456, 2000);
                    break;
                case 0x456:
                    wzoneLanding.AttenVarsByUiid(Data.AIR_UIID);
                    atnhandler.sendEmptyMessageDelayed(0x789, 2000);
                    break;
                case 0x789:
                    wzoneLanding.AttenVarsByUiid(Data.ILL_UIID);
                    atnhandler.sendEmptyMessageDelayed(0x753, 2000);
                    break;
                case 0x753:
                    wzoneLanding.AttenVarsByUiid(Data.SJ_UIID);
                    atnhandler.postDelayed(runnable, 1000);
                    break;
                case 0x4265:
                    temD.setText(reciveBean.getTem() + "℃");
                    humD.setText(reciveBean.getHum() + "%");
                    illD.setText(reciveBean.getIll() + "%");
                    if (Integer.parseInt(reciveBean.getAir()) < 10) {
                        airD.setText("优");
                    } else {
                        airD.setText("差");
                    }
                    if (reciveBean.getSjtr().equals("1")) {
                        wzoneLanding.setKeyByUiid(Data.BB_UIID, Data.BB_OPEN);
                        reciveBean.setSjtr("8");
                    } else if (reciveBean.getSjtr().equals("2")) {
                        car.setText("剩余 " + 5);
                    } else if (reciveBean.getSjtr().equals("0")) {
                        car.setText("剩余 " + 6);
                    }
                    break;
            }
        }
    };

    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            atnhandler.sendEmptyMessage(0x4265);
            atnhandler.postDelayed(this, 1000);
        }
    };
    @BindView(R.id.tem_d)
    TextView temD;
    @BindView(R.id.hum_d)
    TextView humD;
    @BindView(R.id.air_d)
    TextView airD;
    @BindView(R.id.ill_d)
    TextView illD;
    @BindView(R.id.kai)
    ImageView kai;
    @BindView(R.id.kai_str)
    TextView kaiStr;
    @BindView(R.id.she)
    ImageView she;
    @BindView(R.id.she_str)
    TextView sheStr;
    @BindView(R.id.deng)
    TextView deng;
    @BindView(R.id.car)
    TextView car;
    private Landing wzoneLanding;
    private ReciveDateHander reciveDateHander;
    private ReciveBean reciveBean;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        initWZoneSdk();
        SysApplication.getInstance().addActivity(this);
    }

    private void initWZoneSdk() {
        reciveDateHander = (ReciveDateHander) getIntent().getSerializableExtra("DateHandler");
        wzoneLanding = reciveDateHander.getWzoneLanding();
        reciveBean = reciveDateHander.getReciveBean();
    }

    @Override
    protected void onStart() {
        super.onStart();
        atnhandler.sendEmptyMessageDelayed(0x123, 1000);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            new AlertDialog.Builder(this).setTitle("系统提示").setMessage("是否要退出？").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    SysApplication.getInstance().exit();
                }
            }).setNegativeButton("取消", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            }).show();
        }
        return super.onKeyDown(keyCode, event);
    }

    boolean state = false;

    @OnClick({R.id.kai, R.id.kai_str, R.id.she, R.id.she_str, R.id.deng})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.kai:
                break;
            case R.id.kai_str:
                break;
            case R.id.she:
                Intent intent1 = new Intent(this, AddCameraActivity.class);
                startActivity(intent1);
                break;
            case R.id.she_str:
                Intent intent2 = new Intent(this, AddCameraActivity.class);
                startActivity(intent2);
                break;
            case R.id.deng:
                if (state == false) {
                    wzoneLanding.setKeyByUiid(Data.DG_UIID, Data.DG_OPEN);
                    state = true;
                } else {
                    wzoneLanding.setKeyByUiid(Data.DG_UIID, Data.DG_CLOSE);
                    state = false;
                }
                break;
        }
    }
}
