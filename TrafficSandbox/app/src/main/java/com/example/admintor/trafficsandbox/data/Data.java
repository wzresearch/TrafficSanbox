package com.example.admintor.trafficsandbox.data;

/**
 * Created by Admintor on 2017/11/8.
 */

public class Data {
    /**
     * 温湿度检测
     */
    public static final String H_T_UIID = "71001";
    /**
     * 空气质量检测
     */
    public static final String AIR_UIID = "71013";

    /**
     * 光照检测
     */
    public static final String ILL_UIID = "71002";


    /**
     * 数据传输
     */
    public static final String SJ_UIID = "71012";

    /**
     * 语音播报控制
     */
    public static final String BB_UIID = "71014";
    public static final int BB_OPEN = 1;

    /**
     * 灯光控制
     */
    public static final String DG_UIID = "71008";
    public static final int DG_OPEN = 1;
    public static final int DG_CLOSE = 2;

}
