package com.example.admintor.trafficsandbox.data;

/**
 * Created by Admintor on 2018/1/17.
 */

public class ReciveBean {
    private String tem="26℃";
    private String hum="45%";
    private String air="5";
    private String ill="30%";
    private String sjtr="8";

    public String getTem() {
        return tem;
    }

    public void setTem(String tem) {
        this.tem = tem;
    }

    public String getHum() {
        return hum;
    }

    public void setHum(String hum) {
        this.hum = hum;
    }

    public String getAir() {
        return air;
    }

    public void setAir(String air) {
        this.air = air;
    }

    public String getIll() {
        return ill;
    }

    public void setIll(String ill) {
        this.ill = ill;
    }

    public String getSjtr() {
        return sjtr;
    }

    public void setSjtr(String sjtr) {
        this.sjtr = sjtr;
    }
}
