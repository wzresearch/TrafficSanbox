package com.example.admintor.trafficsandbox.data;


import android.content.Context;

import com.example.wzsdk.login.Landing;
import com.example.wzsdk.scan.Search;

import java.io.Serializable;

/**
 * Created by Admintor on 2018/1/17.
 */

public class ReciveDateHander implements Serializable {

    private ReciveBean reciveBean;
    private Search wzoneSearch;
    private Landing wzoneLanding;

    public ReciveDateHander(Context context) {
        reciveBean = new ReciveBean();
        wzoneSearch = new Search("DFPMNAWAW", context);
        wzoneLanding = new Landing("DFPMNAWAW");
    }

    public void ReciverDispose(String uiid, byte b, String index) {
        switch (uiid) {
            case Data.H_T_UIID:
                if (index.equals("1")) {
                    reciveBean.setTem(b + "");
                } else if (index.equals("2")) {
                    reciveBean.setHum(b + "");
                }
                break;

            case Data.AIR_UIID:
                if (index.equals("1")) {
                    reciveBean.setAir(b + "");
                }
                break;
            case Data.ILL_UIID:
                if (index.equals("1")) {
                    reciveBean.setIll(b + "");
                }
                break;
            case Data.SJ_UIID:
                if (index.equals("1")) {
                    reciveBean.setSjtr(b + "");
                }
                break;
        }

    }

    public ReciveBean getReciveBean() {
        return reciveBean;
    }

    public void setReciveBean(ReciveBean reciveBean) {
        this.reciveBean = reciveBean;
    }

    public Search getWzoneSearch() {
        return wzoneSearch;
    }

    public void setWzoneSearch(Search wzoneSearch) {
        this.wzoneSearch = wzoneSearch;
    }

    public Landing getWzoneLanding() {
        return wzoneLanding;
    }

    public void setWzoneLanding(Landing wzoneLanding) {
        this.wzoneLanding = wzoneLanding;
    }
}
