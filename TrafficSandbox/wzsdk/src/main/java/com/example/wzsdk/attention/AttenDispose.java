package com.example.wzsdk.attention;


import com.example.wzsdk.constant.Data;
import com.example.wzsdk.login.Landing;
import com.example.wzsdk.callback.LoginStateCallback;
import com.example.wzsdk.login.SendUtils;
import com.example.wzsdk.tools.ToolsClass;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.util.Arrays;

/**
 * Created by SunLight on 2017/8/15.
 */

public class AttenDispose {
    static Data data;
    int i = 0;
    private SendUtils sendUtils;

    public void disPose(byte[] bytes, Data data, LoginStateCallback loginState) {
        this.data = data;

        if (!VaridToUiid(Zhuan(new byte[]{bytes[24], bytes[25]}, 10)).equals("null")) {
            loginState.AttentionStarting(sendUtils.getList().get(i)[0], bytes[28], IndexStr(bytes), Zhuan(new byte[]{bytes[24], bytes[25]}, 10), ToolsClass.Dut_ID(data.SnHash, 10));

        } else if (!VaridToUiids(Zhuan(new byte[]{bytes[24], bytes[25]}, 10)).equals("null")) {
            loginState.AttentionStarting(sendUtils.getAdminList().get(i)[0], bytes[28], IndexStr(bytes), Zhuan(new byte[]{bytes[24], bytes[25]}, 10), ToolsClass.Dut_ID(data.SnHash, 10));

        } else if (!VaridToUiid(Zhuan(new byte[]{(byte) (bytes[24] + 1), bytes[25]}, 10)).equals("null")) {
            loginState.AttentionStarting(sendUtils.getList().get(i)[0], bytes[28], IndexStr(bytes), Zhuan(new byte[]{bytes[24], bytes[25]}, 10), ToolsClass.Dut_ID(data.SnHash, 10));

        } else {
            loginState.AttentionStarting("null", (byte) 0, "0", "null", ToolsClass.Dut_ID(data.SnHash, 10));
        }
    }

    public void disPoseing(byte[] bytes, Data data, LoginStateCallback loginState) {
        this.data = data;
        if (bytes.length > 40) {
            for (int i = 0; i < sendUtils.getList().size(); i++) {
                if (Zhuan(new byte[]{bytes[20], bytes[21]}, 10).equals(sendUtils.getList().get(i)[5])) {
                    loginState.AttentionProcess(sendUtils.getList().get(i)[0], (byte) 0, toascii(new byte[]{bytes[28], bytes[29], bytes[30], bytes[31], bytes[32], bytes[33], bytes[34], bytes[35]}), IndexStro(bytes), Zhuan(new byte[]{bytes[20], bytes[21]}, 10), ToolsClass.Dut_ID(data.SnHash, 10));
                } else if (Zhuan(new byte[]{(byte) (bytes[20] + 1), bytes[21]}, 10).equals(sendUtils.getList().get(i)[5])) {
                    loginState.AttentionProcess(sendUtils.getList().get(i)[0], (byte) 0, toascii(new byte[]{bytes[28], bytes[29], bytes[30], bytes[31], bytes[32], bytes[33], bytes[34], bytes[35]}), IndexStro(bytes), Zhuan(new byte[]{bytes[20], bytes[21]}, 10), ToolsClass.Dut_ID(data.SnHash, 10));
                }
            }
        } else {
            for (int i = 0; i < sendUtils.getList().size(); i++) {
                if (Zhuan(new byte[]{bytes[20], bytes[21]}, 10).equals(sendUtils.getList().get(i)[5])) {
                    loginState.AttentionProcess(sendUtils.getList().get(i)[0], bytes[26], null, IndexStro(bytes), Zhuan(new byte[]{bytes[20], bytes[21]}, 10), ToolsClass.Dut_ID(data.SnHash, 10));
                }
            }
            for (int i = 0; i < sendUtils.getAdminList().size(); i++) {
                if (Zhuan(new byte[]{bytes[20], bytes[21]}, 10).equals(sendUtils.getAdminList().get(i)[0])) {
                    loginState.AttentionProcess(sendUtils.getAdminList().get(i)[0], bytes[26], null, IndexStro(bytes), Zhuan(new byte[]{bytes[20], bytes[21]}, 10), ToolsClass.Dut_ID(data.SnHash, 10));
                }
            }
        }
    }

    public void setPose(byte[] bytes, Data data, LoginStateCallback loginState) {
        this.data = data;
        data.Number = bytes[34] + "";
        for (int i = 0; i < sendUtils.getList().size(); i++) {
            if (Zhuan(new byte[]{bytes[28], bytes[29]}, 10).equals(sendUtils.getList().get(i)[5])) {
                loginState.SetVarState(sendUtils.getList().get(i)[0], bytes[34], ToolsClass.Dut_ID(data.SnHash, 10));
            }
        }
        for (int i = 0; i < sendUtils.getAdminList().size(); i++) {
            if (Zhuan(new byte[]{bytes[28], bytes[29]}, 10).equals(sendUtils.getAdminList().get(i)[0])) {
                loginState.SetVarState(sendUtils.getAdminList().get(i)[0], bytes[34], ToolsClass.Dut_ID(data.SnHash, 10));
            }
        }
    }

    public void disSetKeying(byte[] bytes, Data data, LoginStateCallback loginState) {
        this.data = data;
        loginState.SetKeyState(ToolsClass.Dut_ID(data.FileID.get(setindexing(bytes)), 10), bytes[7] + "", ToolsClass.Dut_ID(data.SnHash, 10));
    }


    /****************************************************************************************************/

    public AttenDispose(SendUtils sendUtils) {
        this.sendUtils = sendUtils;

    }

    public String VaridToUiid(String Varid) {
        for (int j = 0; j < sendUtils.getList().size(); j++) {
            if (Varid.equals(sendUtils.getList().get(j)[5])) {
                this.i = j;
                return sendUtils.getList().get(j)[0];
            }
        }
        return "null";
    }

    public String VaridToUiids(String varid) {

        for (int j = 0; j < sendUtils.getAdminList().size(); j++) {
            if (varid.equals(sendUtils.getAdminList().get(j)[0])) {
                this.i = j;
                return sendUtils.getAdminList().get(j)[0];
            }
        }
        return "null";
    }

    public String toascii(byte[] bytes) {
        String dd = null;
        try {
            dd = new String(bytes, "ascii");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return dd;
    }

    public String IndexStr(byte[] bytes) {
        for (int i = 0; i < data.Node.size(); i++) {
            if (Zhuan(new byte[]{bytes[24], bytes[25]}, 10).equals(data.Node.get(i)[0])) {
                return data.Node.get(i)[2];
            }
        }
        return "1";
    }

    public String Zhuan(byte[] bytes, int radix) {
        switch (bytes.length) {
            case 2:
                byte[] tb = {bytes[1], bytes[0]};
                return new BigInteger(1, tb).toString(radix);
            case 4:
                byte[] tbs = {bytes[3], bytes[2], bytes[1], bytes[0]};
                return new BigInteger(1, tbs).toString(radix);
        }
        return null;
    }

    public String IndexStro(byte[] bytes) {
        for (int i = 0; i < data.Node.size(); i++) {
            if (Zhuan(new byte[]{bytes[20], bytes[21]}, 10).equals(data.Node.get(i)[0])) {
                return data.Node.get(i)[2];
            }
        }
        return "1";
    }

    public int setindexing(byte[] bytes) {
        int j = 0;
        for (int i = 0; i < data.Id.size(); i++) {
            if (Arrays.equals(new byte[]{bytes[8], bytes[9]}, data.Id.get(i))) {
                j = i;
            } else {
                for (int k = 0; k < data.Sid.size(); k++) {
                    if (Arrays.equals(new byte[]{bytes[8], bytes[9], bytes[10], bytes[11]}, data.Sid.get(k)) && Arrays.equals(data.MainDevID.get(k), data.Id.get(i))) {
                        j = i;
                    }
                }
            }
        }
        return j;
    }

}
