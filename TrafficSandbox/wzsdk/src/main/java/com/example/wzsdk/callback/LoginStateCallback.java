package com.example.wzsdk.callback;


/**
 * Created by SunLight on 2017/8/14.
 */

public interface LoginStateCallback {
    void LoginStatus(boolean state, String sn);//登陆状态回调

    void AttentionStarting(String type, byte var, String index, String id, String sn);//关注状态回调

    void AttentionProcess(String type, byte var, String str, String index, String id, String sn);//关注数据改变回调

    void SetVarState(String type, byte var, String sn);//设置值改变回调

    void SetKeyState(String type,String state, String sn);//设置值改变回调
}
