package com.example.wzsdk.callback;

import java.util.List;

/**
 * Created by Admintor on 2018/1/5.
 */

public interface PingSuccessCallback {
    void onSuccess(List<String> strings);
}
