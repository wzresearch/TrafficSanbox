package com.example.wzsdk.callback;

/**
 * Created by SunLight on 2017/8/11.
 */

public interface ReceiveData {
    void receive(byte[] number);

}
