package com.example.wzsdk.callback;

import java.util.List;

/**
 * Created by SunLight on 2017/8/29.
 */

public interface SearchFinish {
    void onSearchFinsh(List<String[]> list);
}
