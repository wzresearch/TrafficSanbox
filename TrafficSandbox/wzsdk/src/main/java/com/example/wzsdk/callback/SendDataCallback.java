package com.example.wzsdk.callback;

import com.example.wzsdk.login.LoginThread;

/**
 * Created by Admintor on 2018/1/10.
 */

public interface SendDataCallback {
    void sendData(byte[] number);

    void sendDataSuccess(int state);
}
