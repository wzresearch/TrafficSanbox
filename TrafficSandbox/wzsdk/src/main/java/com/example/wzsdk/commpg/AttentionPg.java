package com.example.wzsdk.commpg;

/**
 * Created by SunLight on 2017/6/23.
 */

public class AttentionPg {
    public  byte[] Num;
    public  byte[] a;
    public  byte[] Datalen;
    public  byte[] APP_ATTENTION_ACT;
    public  byte[] APP_ATTENTION_TYPE;
    public  byte[] b;
    public  byte[] ObjID;
    public  byte[] Param;
    public  byte[] Data1;
    public  byte[] Data2;

    public  byte[][] Attention = {
            Num = new byte[1],//item个数
            a = new byte[1],
            Datalen = new byte[2],
            APP_ATTENTION_ACT = new byte[1],//订阅（开始，取消）
            APP_ATTENTION_TYPE = new byte[1],//事件（设备，变量）
            b = new byte[2],
            ObjID = new byte[4],
            Param = new byte[4],
            Data1 = new byte[2],
            Data2 = new byte[2]//校验位
    };
}
