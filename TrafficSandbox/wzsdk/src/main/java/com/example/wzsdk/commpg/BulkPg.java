package com.example.wzsdk.commpg;

/**
 * Created by SunLight on 2017/6/13.
 */

public class BulkPg {
    public  byte[] BULK_OPERATE_PKT_TYPE;//动作
    public  byte[] INFO_TYPE;//指定操作对象
    public  byte[] Idx;
    public  byte[] Num;
    public  byte[] Num2;
    public  byte[] Data1;//实体，必须4字节对齐
    public  byte[] Data2;//实体，必须4字节对齐
    public  byte[][] Bulk = {
            BULK_OPERATE_PKT_TYPE = new byte[1],//动作
            INFO_TYPE = new byte[1],//指定操作对象
            Idx = new byte[2],
            Num = new byte[2],
            Num2 = new byte[2],
            Data1 = new byte[2],//实体，必须4字节对齐
            Data2 = new byte[2]//实体，必须4字节对齐

    };
}
