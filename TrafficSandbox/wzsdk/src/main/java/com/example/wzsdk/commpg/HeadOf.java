package com.example.wzsdk.commpg;

/**
 * Created by SunLight on 2017/6/13.
 */

public class HeadOf {
    public  byte[] PktLen;//数据区长度，包含最后两个字节的校验码
    public  byte[] _PktLen;//数据区长度保密码
    public  byte[] PktSn;//包序列号
    public  byte[] GLOBA_PKT_TYPE; //此包的类型
    public  byte[] GLOBA_PKT_RESULT;   //0表示是主包，非0表示是回包，并表示具体的回复码
    public  byte[] DutID; //fly的id
    public  byte[] AppID; //app连接的id

    public  byte[][] Head = {
            //数据区长度，包含最后两个字节的校验码
            PktLen = new byte[2],
            //数据区长度保密码
            _PktLen = new byte[2],
            //包序列号
            PktSn = new byte[2],
            //此包的类型
            GLOBA_PKT_TYPE = new byte[1],
            //0表示是主包，非0表示是回包，并表示具体的回复码
            GLOBA_PKT_RESULT = new byte[1],
            //fly的id
            DutID = new byte[4],
            //app连接的id
            AppID = new byte[4],
    };
}
