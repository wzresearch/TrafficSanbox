package com.example.wzsdk.commpg;

/**
 * Created by SunLight on 2017/6/19.
 */

public class KeyPg {
    public  byte[] Num;//Data里SCENE_ITEM的个数
    public  byte[] a;
    public  byte[] b;
    public  byte[] SCENE_ITEM_ACT;//动作
    public  byte[] a1;
    public  byte[] DelaySec;//执行完毕后的延时
    public  byte[] DevID;//输出设备
    public  byte[] Key;
    public  byte[] SCENE_ITEM;
    public  byte[] Data1; //存放包检验
    public  byte[] Data2; //存放包检验

    public  byte[][] key = {
            Num = new byte[1],
            a = new byte[1],
            b = new byte[2],
            SCENE_ITEM_ACT = new byte[1],
            a1 = new byte[1],
            DelaySec = new byte[2],

            DevID = new byte[4],
            Key = new byte[1],
            SCENE_ITEM = new byte[7],


            Data1 = new byte[2],
            Data2 = new byte[2]
    };
}
