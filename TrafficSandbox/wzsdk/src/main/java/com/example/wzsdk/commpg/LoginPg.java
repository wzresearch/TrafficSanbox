package com.example.wzsdk.commpg;

/**
 * Created by SunLight on 2017/6/13.
 */

public class LoginPg {

    public  byte[] APP_CLIENT_TYPE; //登录类型，返回时带回客户端索引，客户端计算PktSn时必须叠加此索引到高4位。
    public  byte[] VendorID; //厂商号码
    public  byte[] FwVer;  //固件版本
    public  byte[] SnHash;//序列号做MakeHash后的值
    public  byte[] PwHash; //密码做MakeHash后的值
    public  byte[] SecretKey;  //返回的密钥
    public  byte[] Role; //角色
    public  byte[] SysConfigFlag; //返回信息配置标识，不允许为0
    public  byte[] InfoConfigFlag; //返回系统参数配置标识，不允许为0
    public  byte[] Data1; //存放包检验
    public  byte[] Data2; //存放包检验

    public  byte[][] login = {
            //登录类型，返回时带回客户端索引，客户端计算PktSn时必须叠加此索引到高4位。
            APP_CLIENT_TYPE = new byte[1],
            //厂商号码
            VendorID = new byte[1],
            //固件版本
            FwVer = new byte[2],
            //序列号做MakeHash后的值
            SnHash = new byte[4],
            //密码做MakeHash后的值
            PwHash = new byte[4],
            //返回的密钥
            SecretKey = new byte[2],
            //角色
            Role = new byte[2],
            //返回信息配置标识，不允许为0
            SysConfigFlag = new byte[4],
            //返回系统参数配置标识，不允许为0
            InfoConfigFlag = new byte[64],
            //存放包检验
            Data1 = new byte[2],
            Data2 = new byte[2]


    };


}
