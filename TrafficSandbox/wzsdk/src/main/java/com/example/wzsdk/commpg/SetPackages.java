package com.example.wzsdk.commpg;

import android.util.Log;
import android.view.KeyEvent;

import com.example.wzsdk.constant.Data;
import com.example.wzsdk.tools.ToolsClass;

import java.math.BigInteger;
import java.util.Arrays;

/**
 * Created by SunLight on 2017/8/11.
 */

public class SetPackages {
    static public final int SINGLE_BYTE_MASK = 0xFF;

    private HeadOf headOf;
    private LoginPg loginPg;
    private BulkPg bulkPg;
    private BeatPg beatPg;
    private AttentionPg attentionPg;
    private VariablePg variablePg;
    private KeyPg keyPg;
    private Data data;


    public SetPackages() {
        data = new Data();
        headOf = new HeadOf();
        loginPg = new LoginPg();
        bulkPg = new BulkPg();
        beatPg = new BeatPg();
        keyPg = new KeyPg();
        attentionPg = new AttentionPg();
        variablePg = new VariablePg();
    }


    /**
     * @User SunLight
     * @Data 2018/1/11 15:51
     * @Method 搜索组包
     **/
    public byte[] ScanPg() {
        headOf.PktLen[0] = ToolsClass.TwoArraysLenght(headOf.Head, loginPg.login);
        ToolsClass.ArraysCopy(headOf._PktLen, ToolsClass.LenghtToBytes(ToolsClass.TwoArraysLenght(headOf.Head, loginPg.login)));
        headOf.PktSn[1] = 1;
        headOf.GLOBA_PKT_TYPE[0] = 8;
        headOf.GLOBA_PKT_RESULT[0] = 0;
        loginPg.APP_CLIENT_TYPE[0] = 1;
        ToolsClass.ArraysCopy(headOf.AppID, data.AppId);
        for (int i = 0; i < 4; i++) {
            loginPg.SnHash[i] = -1;
            loginPg.PwHash[i] = -1;
            headOf.DutID[i] = -1;
        }
        ToolsClass.ArraysCopy(loginPg.Data2, ToolsClass.makeHash(Arrays.copyOf(ToolsClass.TwoArraysMerge(headOf.Head, loginPg.login), ToolsClass.TwoArraysMerge(headOf.Head, loginPg.login).length - 2), 0, ToolsClass.TwoArraysMerge(headOf.Head, loginPg.login).length - 2, (short) 0));

        return ToolsClass.TwoArraysMerge(headOf.Head, loginPg.login);
    }

    /**
     * @User SunLight
     * @Data 2018/1/11 15:52
     * @Method 登录组包
     **/
    public byte[] LoginPg() {
        headOf.PktLen[0] = ToolsClass.TwoArraysLenght(headOf.Head, loginPg.login);
        ToolsClass.ArraysCopy(headOf._PktLen, ToolsClass.LenghtToBytes(ToolsClass.TwoArraysLenght(headOf.Head, loginPg.login)));
        headOf.PktSn[1] = 2;
        headOf.GLOBA_PKT_TYPE[0] = 8;
        headOf.GLOBA_PKT_RESULT[0] = 0;
        loginPg.APP_CLIENT_TYPE[0] = 1;
        ToolsClass.ArraysCopy(headOf.AppID, data.AppId);

        headOf.DutID = new byte[]{-1, -1, -1, -1};
        System.arraycopy(data.SnHash, 0, loginPg.SnHash, 0, 4);
        System.arraycopy(data.paworld, 0, loginPg.PwHash, 0, 4);
        ToolsClass.ArraysCopy(loginPg.Data2, ToolsClass.makeHash(Arrays.copyOf(ToolsClass.TwoArraysMerge(headOf.Head, loginPg.login), ToolsClass.TwoArraysMerge(headOf.Head, loginPg.login).length - 2), 0, ToolsClass.TwoArraysMerge(headOf.Head, loginPg.login).length - 2, (short) 0));
        return ToolsClass.TwoArraysMerge(headOf.Head, loginPg.login);
    }

    /**
     * @User SunLight
     * @Data 2018/1/11 15:52
     * @Method 系统参数组包
     **/
    public byte[] BulkPg() {
        headOf.PktLen[0] = ToolsClass.TwoArraysLenght(headOf.Head, bulkPg.Bulk);
        ToolsClass.ArraysCopy(headOf._PktLen, ToolsClass.LenghtToBytes(ToolsClass.TwoArraysLenght(headOf.Head, bulkPg.Bulk)));
        headOf.PktSn[1] = 3;
        headOf.GLOBA_PKT_TYPE[0] = 2;
        headOf.GLOBA_PKT_RESULT[0] = 0;

        bulkPg.BULK_OPERATE_PKT_TYPE[0] = 0;
        bulkPg.INFO_TYPE[0] = 0;
        bulkPg.Idx[0] = 0;
        ToolsClass.ArraysCopy(bulkPg.Num, new byte[]{0, 0});
        ToolsClass.ArraysCopy(bulkPg.Data2, ToolsClass.makeHash(Arrays.copyOf(ToolsClass.TwoArraysMerge(headOf.Head, bulkPg.Bulk), ToolsClass.TwoArraysMerge(headOf.Head, bulkPg.Bulk).length - 2), 0, ToolsClass.TwoArraysMerge(headOf.Head, bulkPg.Bulk).length - 2, ToolsClass.SecretKey(data.SecretKey)));
        return ToolsClass.TwoArraysMerge(headOf.Head, bulkPg.Bulk);
    }

    public byte[] BulkPg(int num, byte startIndex, byte endIndex, byte sn) {
        switch (num) {
            case 4:
                headOf.PktLen[0] = ToolsClass.TwoArraysLenght(headOf.Head, bulkPg.Bulk);
                ToolsClass.ArraysCopy(headOf._PktLen, ToolsClass.LenghtToBytes(ToolsClass.TwoArraysLenght(headOf.Head, bulkPg.Bulk)));
                headOf.PktSn[1] = sn;
                headOf.GLOBA_PKT_TYPE[0] = 2;
                headOf.GLOBA_PKT_RESULT[0] = 0;

                bulkPg.BULK_OPERATE_PKT_TYPE[0] = 1;
                bulkPg.INFO_TYPE[0] = (byte) num;
                bulkPg.Idx[0] = startIndex;
                ToolsClass.ArraysCopy(bulkPg.Num, new byte[]{endIndex, 0});
                ToolsClass.ArraysCopy(bulkPg.Data2, ToolsClass.makeHash(Arrays.copyOf(ToolsClass.TwoArraysMerge(headOf.Head, bulkPg.Bulk), ToolsClass.TwoArraysMerge(headOf.Head, bulkPg.Bulk).length - 2), 0, ToolsClass.TwoArraysMerge(headOf.Head, bulkPg.Bulk).length - 2, ToolsClass.SecretKey(data.SecretKey)));

                return ToolsClass.TwoArraysMerge(headOf.Head, bulkPg.Bulk);
            case 1:
                headOf.PktLen[0] = ToolsClass.TwoArraysLenght(headOf.Head, bulkPg.Bulk);
                ToolsClass.ArraysCopy(headOf._PktLen, ToolsClass.LenghtToBytes(ToolsClass.TwoArraysLenght(headOf.Head, bulkPg.Bulk)));
                headOf.PktSn[1] = sn;
                headOf.GLOBA_PKT_TYPE[0] = 2;
                headOf.GLOBA_PKT_RESULT[0] = 0;

                bulkPg.BULK_OPERATE_PKT_TYPE[0] = 1;
                bulkPg.INFO_TYPE[0] = (byte) num;
                bulkPg.Idx[0] = 1;
                ToolsClass.ArraysCopy(bulkPg.Num, data.Length);
//                    Number.setLenght(new byte[]{Number.getMessage()[34], Number.getMessage()[35]});
                ToolsClass.ArraysCopy(bulkPg.Data2, ToolsClass.makeHash(Arrays.copyOf(ToolsClass.TwoArraysMerge(headOf.Head, bulkPg.Bulk), ToolsClass.TwoArraysMerge(headOf.Head, bulkPg.Bulk).length - 2), 0, ToolsClass.TwoArraysMerge(headOf.Head, bulkPg.Bulk).length - 2, ToolsClass.SecretKey(data.SecretKey)));
                return ToolsClass.TwoArraysMerge(headOf.Head, bulkPg.Bulk);

            case 2:

                headOf.PktLen[0] = ToolsClass.TwoArraysLenght(headOf.Head, bulkPg.Bulk);
                ToolsClass.ArraysCopy(headOf._PktLen, ToolsClass.LenghtToBytes(ToolsClass.TwoArraysLenght(headOf.Head, bulkPg.Bulk)));
                headOf.PktSn[1] = 5;
                headOf.GLOBA_PKT_TYPE[0] = 2;
                headOf.GLOBA_PKT_RESULT[0] = 0;

                bulkPg.BULK_OPERATE_PKT_TYPE[0] = 1;
                bulkPg.INFO_TYPE[0] = (byte) 1;
                bulkPg.Idx[0] = 71;
                ToolsClass.ArraysCopy(bulkPg.Num, new byte[]{(byte) (data.Length[0] - bulkPg.Idx[0]), 0});
                ToolsClass.ArraysCopy(bulkPg.Data2, ToolsClass.makeHash(Arrays.copyOf(ToolsClass.TwoArraysMerge(headOf.Head, bulkPg.Bulk), ToolsClass.TwoArraysMerge(headOf.Head, bulkPg.Bulk).length - 2), 0, ToolsClass.TwoArraysMerge(headOf.Head, bulkPg.Bulk).length - 2, ToolsClass.SecretKey(data.SecretKey)));

                return ToolsClass.TwoArraysMerge(headOf.Head, bulkPg.Bulk);
        }
        return null;
    }

    /**
     * @User SunLight
     * @Data 2018/1/11 15:53
     * @Method 关注组包
     **/
    public byte[] AttentionPg(int type, int act, int position) {
        headOf.PktLen[0] = ToolsClass.TwoArraysLenght(headOf.Head, attentionPg.Attention);
        ToolsClass.ArraysCopy(headOf._PktLen, ToolsClass.LenghtToBytes(ToolsClass.TwoArraysLenght(headOf.Head, attentionPg.Attention)));
        headOf.PktSn[1] = 1;
        headOf.GLOBA_PKT_TYPE[0] = 16;
        headOf.GLOBA_PKT_RESULT[0] = 0;

        attentionPg.Num[0] = 1;
        attentionPg.APP_ATTENTION_ACT[0] = (byte) type;
        attentionPg.APP_ATTENTION_TYPE[0] = (byte) act;
        attentionPg.Datalen[0] = 12;

        if (act == 3) {
            ToolsClass.ArraysCopy(attentionPg.ObjID, data.Sid.get(position));
            attentionPg.ObjID[2] = 4;
            ToolsClass.ArraysCopy(attentionPg.Data2, ToolsClass.makeHash(Arrays.copyOf(ToolsClass.TwoArraysMerge(headOf.Head, attentionPg.Attention), ToolsClass.TwoArraysMerge(headOf.Head, attentionPg.Attention).length - 2), 0, ToolsClass.TwoArraysMerge(headOf.Head, attentionPg.Attention).length - 2, ToolsClass.SecretKey(data.SecretKey)));
        } else if (act == 2) {
            ToolsClass.ArraysCopy(attentionPg.ObjID, data.Id.get(position));
            attentionPg.ObjID[2] = 4;
            ToolsClass.ArraysCopy(attentionPg.Data2, ToolsClass.makeHash(Arrays.copyOf(ToolsClass.TwoArraysMerge(headOf.Head, attentionPg.Attention), ToolsClass.TwoArraysMerge(headOf.Head, attentionPg.Attention).length - 2), 0, ToolsClass.TwoArraysMerge(headOf.Head, attentionPg.Attention).length - 2, ToolsClass.SecretKey(data.SecretKey)));
        } else {
            ToolsClass.ArraysCopy(attentionPg.ObjID, new byte[]{0, 0, 0, 0});
            attentionPg.ObjID[2] = 4;
            ToolsClass.ArraysCopy(attentionPg.Data2, ToolsClass.makeHash(Arrays.copyOf(ToolsClass.TwoArraysMerge(headOf.Head, attentionPg.Attention), ToolsClass.TwoArraysMerge(headOf.Head, attentionPg.Attention).length - 2), 0, ToolsClass.TwoArraysMerge(headOf.Head, attentionPg.Attention).length - 2, ToolsClass.SecretKey(data.SecretKey)));
        }
        return ToolsClass.TwoArraysMerge(headOf.Head, attentionPg.Attention);
    }

    /**
     * @User SunLight
     * @Data 2018/1/11 15:53
     * @Method 变量组包
     **/
    public byte[] VariablePg(int position, int num) {
        headOf.PktLen[0] = ToolsClass.TwoArraysLenght(headOf.Head, variablePg.variable);
        ToolsClass.ArraysCopy(headOf._PktLen, ToolsClass.LenghtToBytes(ToolsClass.TwoArraysLenght(headOf.Head, variablePg.variable)));
        headOf.PktSn[1] = 1;
        headOf.GLOBA_PKT_TYPE[0] = 11;
        headOf.GLOBA_PKT_RESULT[0] = 0;
        variablePg.Num[0] = 1;
        ToolsClass.ArraysCopy(headOf.AppID, data.AppId);
        ToolsClass.ArraysCopy(variablePg.VarID, data.Sid.get(position));
        variablePg.VARI_GET_ACT_VARI_GET_METHOD[0] = 04;
        variablePg.VarVal[0] = (byte) num;
        ToolsClass.ArraysCopy(variablePg.Data2, ToolsClass.makeHash(Arrays.copyOf(ToolsClass.TwoArraysMerge(headOf.Head, variablePg.variable), ToolsClass.TwoArraysMerge(headOf.Head, variablePg.variable).length - 2), 0, ToolsClass.TwoArraysMerge(headOf.Head, variablePg.variable).length - 2, ToolsClass.SecretKey(data.SecretKey)));
        return ToolsClass.TwoArraysMerge(headOf.Head, variablePg.variable);
    }

    public byte[] VariablePg(int position) {
        headOf.PktLen[0] = ToolsClass.TwoArraysLenght(headOf.Head, variablePg.variable);
        ToolsClass.ArraysCopy(headOf._PktLen, ToolsClass.LenghtToBytes(ToolsClass.TwoArraysLenght(headOf.Head, variablePg.variable)));
        headOf.PktSn[1] = 1;
        headOf.GLOBA_PKT_TYPE[0] = 11;
        headOf.GLOBA_PKT_RESULT[0] = 0;
        variablePg.Num[0] = 1;
        ToolsClass.ArraysCopy(headOf.AppID, data.AppId);
        ToolsClass.ArraysCopy(variablePg.VarID, data.Sid.get(position));
        variablePg.VARI_GET_ACT_VARI_GET_METHOD[0] = 01;
//        variablePg.VarVal[0] = (byte) num;
        ToolsClass.ArraysCopy(variablePg.Data2, ToolsClass.makeHash(Arrays.copyOf(ToolsClass.TwoArraysMerge(headOf.Head, variablePg.variable), ToolsClass.TwoArraysMerge(headOf.Head, variablePg.variable).length - 2), 0, ToolsClass.TwoArraysMerge(headOf.Head, variablePg.variable).length - 2, ToolsClass.SecretKey(data.SecretKey)));
        return ToolsClass.TwoArraysMerge(headOf.Head, variablePg.variable);
    }


    /**
     * @User SunLight
     * @Data 2018/1/11 15:54
     * @Method 心跳组包
     **/
    public byte[] HeartPg() {
        headOf.PktLen[0] = ToolsClass.TwoArraysLenght(headOf.Head, beatPg.beat);
        ToolsClass.ArraysCopy(headOf._PktLen, ToolsClass.LenghtToBytes(ToolsClass.TwoArraysLenght(headOf.Head, beatPg.beat)));
        headOf.PktSn[1] = 1;
        headOf.GLOBA_PKT_TYPE[0] = 14;
        headOf.GLOBA_PKT_RESULT[0] = 0;
        ToolsClass.ArraysCopy(beatPg.Data2, ToolsClass.makeHash(Arrays.copyOf(ToolsClass.TwoArraysMerge(headOf.Head, beatPg.beat), ToolsClass.TwoArraysMerge(headOf.Head, beatPg.beat).length - 2), 0, ToolsClass.TwoArraysMerge(headOf.Head, beatPg.beat).length - 2, (short) 0));
        return ToolsClass.TwoArraysMerge(headOf.Head, beatPg.beat);
    }

    /**
     * @User SunLight
     * @Data 2018/1/11 15:54
     * @Method 键值组包
     **/
    public byte[] KeyPg(int position, int num) {
        headOf = new HeadOf();
        ToolsClass.ArraysCopy(headOf.AppID, data.AppId);
        headOf.PktLen[0] = ToolsClass.TwoArraysLenght(headOf.Head, keyPg.key);
        ToolsClass.ArraysCopy(headOf._PktLen, ToolsClass.LenghtToBytes(ToolsClass.TwoArraysLenght(headOf.Head, keyPg.key)));
        for (int i = 0; i < data.Id.get(position).length; i++) {
            headOf.DutID[i] = data.Id.get(position)[i];
        }
        headOf.DutID[2] = 4;
        headOf.PktSn[1] = 1;
        headOf.GLOBA_PKT_TYPE[0] = 6;
        headOf.GLOBA_PKT_RESULT[0] = 0;

        keyPg.Num[0] = 1;
        keyPg.SCENE_ITEM_ACT[0] = 1;
        ToolsClass.ArraysCopy(keyPg.DevID, data.Id.get(position));
        keyPg.DevID[2] = 04;
        keyPg.Key[0] = (byte) num;
        ToolsClass.ArraysCopy(keyPg.Data2, ToolsClass.makeHash(Arrays.copyOf(ToolsClass.TwoArraysMerge(headOf.Head, keyPg.key), ToolsClass.TwoArraysMerge(headOf.Head, keyPg.key).length - 2), 0, ToolsClass.TwoArraysMerge(headOf.Head, keyPg.key).length - 2, ToolsClass.SecretKey(data.SecretKey)));
        return ToolsClass.TwoArraysMerge(headOf.Head, keyPg.key);
    }

    public Data getData() {
        return data;
    }
}
