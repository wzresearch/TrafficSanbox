package com.example.wzsdk.commpg;

/**
 * Created by SunLight on 2017/6/23.
 */

public class UpdatePg {
    public  byte[] UPDATE_PKT_ACT;
    public  byte[] Num;
    public  byte[] DataLen;
    public  byte[] DevAddr;
    public  byte[] HostAddr;
    public  byte[] Data1;
    public  byte[] Data2;

    public  byte[][] Update = {
            UPDATE_PKT_ACT = new byte[1],
            Num = new byte[1],
            DataLen = new byte[2],
            DevAddr = new byte[4],
            HostAddr = new byte[4],
            Data1 = new byte[2],
            Data2 = new byte[2]
    };

}
