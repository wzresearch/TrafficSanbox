package com.example.wzsdk.commpg;

/**
 * Created by SunLight on 2017/6/14.
 */

public class VariablePg {
    public  byte[] VARI_PKT_CMD; //特殊指令
    public  byte[] Num;//VARI_PKT_ITEM个数
    public  byte[] Num2;
    public  byte[] VARI_GET_BY_TAG;//通过tag方法，到主机里找tag相同的变量，系统变量，用户变量用这种方法
    public  byte[] VARI_GET_BY_DEV;//通过dev方法，到主机里找dev对应的自身变量或者关联变量.设备变量用这种方法
    public  byte[] VarID;//变量id，返回值，返回0表示未成功
    public  byte[] VARI_GET_ACT_VARI_GET_METHOD;//操作
    public  byte[] VAR_STATE;//返回的变量状态
    public  byte[] VarVal;//设置或返回的变量值
    public  byte[] Data1;//实体，必须4字节对齐
    public  byte[] Data2;//校验位

    public  byte[][] variable = {
            VARI_PKT_CMD = new byte[1],
            Num = new byte[1],
            Num2 = new byte[2],
            VARI_GET_BY_TAG = new byte[4],
            VARI_GET_BY_DEV = new byte[4],
            VarID = new byte[4],
            VARI_GET_ACT_VARI_GET_METHOD = new byte[1],
            VAR_STATE = new byte[1],
            VarVal = new byte[2],
            Data1 = new byte[2],
            Data2 = new byte[2],
    };

}
