package com.example.wzsdk.constant;

/**
 * Created by Admintor on 2018/1/11.
 */

public class ClientState {

    public static final int Disconnected = 0;  //断开连接
    public static final int Connecting = Disconnected + 1;      //连接中
    public static final int Connected = Connecting + 1;         //已连接
}
