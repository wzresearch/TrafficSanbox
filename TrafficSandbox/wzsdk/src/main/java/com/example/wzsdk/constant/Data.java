package com.example.wzsdk.constant;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by SunLight on 2017/8/11.
 */

public class Data {
    public static String Number = "";
    public byte[] AppId = new byte[4];
    public byte[] SnHash = {-1, -1, -1, -1};
    public byte[] paworld = {(byte) 0x8f, (byte) 0xf7, 0x78, 0x10}; //密码
    public byte[] SecretKey = new byte[2];//密钥
    public byte[] BulkNum = new byte[2];//
    public byte[] Length = new byte[2];//

    public List<byte[]> Id = new ArrayList<>();//BYTE形id
    public List<String> Id_Str = new ArrayList<>();//字符转形id
    public List<Byte> State = new ArrayList<>();//节点状态
    public List<byte[]> FileID = new ArrayList<>();//FileID


    public List<byte[]> Sid = new ArrayList<>();

    public List<byte[]> MainDevID = new ArrayList<>();
    public List<String> Name = new ArrayList<>();
    public List<Byte> VarIdx = new ArrayList<>();

    public List<Integer> I = new ArrayList<>();
    public List<Integer> J = new ArrayList<>();
    public List<String[]> Node = new ArrayList<>();
    public List<String[]> NodeDev = new ArrayList<>();


    public int isState = ClientState.Disconnected;

    public void ListClear() {
        isState = ClientState.Disconnected;
        Id.clear();
        Id_Str.clear();
        State.clear();
        FileID.clear();
        Sid.clear();
        MainDevID.clear();
        Name.clear();
        VarIdx.clear();
        I.clear();
        J.clear();
        Node.clear();
        NodeDev.clear();
    }


}
