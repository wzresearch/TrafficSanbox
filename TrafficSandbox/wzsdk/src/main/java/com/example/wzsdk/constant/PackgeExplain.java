package com.example.wzsdk.constant;

/**
 * Created by Admintor on 2018/1/11.
 */

public class PackgeExplain {

    public static final int PT_SRV_LOGIN = 8;//登录包

    public static final int PT_BULK_OPERATE = 2;//info批量操作

    public static final int PT_BULK_FOLLOW = 16;//关注包回包（关注是否成功）

    public static final int PT_BULK_FOLLOWING = 12;//关注包（进行中）

    public static final int PT_BULK_KEY_SUCCESS = 11;//设置值回报(设值是否成功)

    public static final int PT_BULK_KEY_BACK = 6;//键值回包

    public static final int PT_BULK_SET_KEY = 9;


}
