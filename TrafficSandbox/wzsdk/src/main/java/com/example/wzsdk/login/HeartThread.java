package com.example.wzsdk.login;

import android.util.Log;

import com.example.wzsdk.commpg.SetPackages;
import com.example.wzsdk.constant.ClientState;
import com.example.wzsdk.constant.Data;

import java.util.Date;

/**
 * Created by SunLight on 2017/8/14.
 */

public class HeartThread extends Thread {
    private Date starDate, endDate;
    private long startime, endtime;
    private long difference;
    private SetPackages setPackages;
    private LoginThread loginThread;

    public HeartThread(SetPackages setPackages, LoginThread loginThread) {
        this.setPackages = setPackages;
        this.loginThread = loginThread;
    }

    @Override
    public void run() {
        starDate = new Date(System.currentTimeMillis());
        startime = starDate.getTime();
        try {
            while (setPackages.getData().isState == ClientState.Connected) {
                Thread.sleep(1000);
                endDate = new Date(System.currentTimeMillis());
                endtime = endDate.getTime();
                difference = (endtime - startime) / 1000;
                if (difference >= 30) {
                    loginThread.Send(setPackages.HeartPg());
                    starDate = new Date(System.currentTimeMillis());
                    startime = starDate.getTime();
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
