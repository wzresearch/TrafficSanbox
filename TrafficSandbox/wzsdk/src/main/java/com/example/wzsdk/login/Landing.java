package com.example.wzsdk.login;

import com.example.wzsdk.callback.LoginStateCallback;
import com.example.wzsdk.commpg.SetPackages;
import com.example.wzsdk.constant.ClientState;
import com.example.wzsdk.tools.ToolsClass;

import java.util.List;

/**
 * Created by SunLight on 2017/8/11.
 */

public class Landing {
    private SendUtils sendUtils;
    private LoginStateCallback loginState;
    private SetPackages setPackages;
    private List<String[]> list;    //主机下节点设备变量列表
    private List<String[]> adminList;    //系统变量列表
    private List<String[]> n_list;
    private List<String[]> n_admin;
    private ReciverData reciver;
    private SendData sendData;
    private LoginThread loginThread = null;
    private HeartThread heartThread = null;


    public Landing(String AppId) {
        init();
        ToolsClass.WriteToId(setPackages.getData().AppId, AppId);
    }

    /**
     * @User SunLight
     * @Data 2018/1/11 15:57
     * @Method 初始化变量
     **/
    private void init() {
        sendUtils = new SendUtils();
        setPackages = sendUtils.getSetPackages();
        list = sendUtils.getList();
        adminList = sendUtils.getAdminList();
        n_list = sendUtils.getN_list();
        n_admin = sendUtils.getN_admin();
    }


    /**
     * @param targetInfo 主机配置类
     * @User SunLight
     * @Data 2018/1/11 15:57
     * @Method 默认登陆方法(默认密码888888)
     **/
    public void WzoneLogin(TargetInfo targetInfo) {
        if (setPackages.getData().isState == ClientState.Disconnected) {
            loginThread = new LoginThread(targetInfo, setPackages, loginState);
            heartThread = new HeartThread(setPackages, loginThread);
            sendData = new SendData(sendUtils, loginState, heartThread);
            reciver = new ReciverData(sendData, sendUtils, loginState);
            loginThread.setReceiveData(reciver);
            sendData.setLoginThread(loginThread);
            loginThread.start();
        }
    }


    /**
     * @User SunLight
     * @Data 2018/1/11 15:58
     * @Method 退出登录方法
     **/
    public void exitLogin() {
        if (setPackages.getData().isState == ClientState.Connected || setPackages.getData().isState == ClientState.Connecting) {
            CancleAllAtten();
            loginThread.disconnect();
            loginState.LoginStatus(false, ToolsClass.Dut_ID(setPackages.getData().SnHash, 10));
            setPackages.getData().ListClear();
        }
    }

    /**
     * @User SunLight
     * @Data 2018/1/11 15:58
     * @Method 数据回调接口
     **/
    public void setLoginListener(LoginStateCallback loginState) {
        this.loginState = loginState;
    }

    /**
     * @User SunLight
     * @Data 2018/1/11 15:58
     * @Method 获取节点设备变量列表方法
     **/
    public List<String[]> getDateList() {
        return n_list;
    }

    /**
     * @User SunLight
     * @Data 2018/1/11 15:58
     * @Method 获取系统变量列表方法
     **/
    public List<String[]> getGlobalVarList() {
        return n_admin;
    }

    /**
     * @param uuid 节点设备ID
     * @User SunLight
     * @Data 2018/1/11 15:58
     * @Method 关注变量方法（仅适用于Uiid为唯一值）
     **/
    public void AttenVarsByUiid(final String uuid) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < list.size(); i++) {
                    if (list.get(i)[0].equals(uuid) && !list.get(i)[2].equals("null")) {
                        final int finalI = i;
                        try {
                            loginThread.Send(setPackages.AttentionPg(1, 3, Integer.parseInt(list.get(finalI)[2])));
                            Thread.sleep(250);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
                for (int i = 0; i < adminList.size(); i++) {
                    if (adminList.get(i)[0].equals(uuid)) {
                        final int finalI = i;
                        loginThread.Send(setPackages.AttentionPg(1, 3, Integer.parseInt(adminList.get(finalI)[2])));
                    }
                }

            }
        }).start();
    }

    /**
     * @param varid 节点变量ID
     * @User SunLight
     * @Data 2018/1/11 15:59
     * @Method 关注变量方法（适用于Uiid不唯一）
     **/
    public void AttenVarByVarid(final String varid) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < list.size(); i++) {
                    if (list.get(i)[5].equals(varid) && !list.get(i)[2].equals("null")) {
                        final int finalI = i;
                        loginThread.Send(setPackages.AttentionPg(1, 3, Integer.parseInt(list.get(finalI)[2])));
                    }
                }
                for (int i = 0; i < adminList.size(); i++) {
                    if (adminList.get(i)[0].equals(varid)) {
                        final int finalI = i;
                        loginThread.Send(setPackages.AttentionPg(1, 3, Integer.parseInt(adminList.get(finalI)[2])));
                    }
                }
            }
        }).start();

    }

    /**
     * @param uuid 节点设备ID
     * @User SunLight
     * @Data 2018/1/11 15:59
     * @Method 取消变量关注方法
     **/
    public void CancleVarsByUiid(final String uuid) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < list.size(); i++) {
                    if (list.get(i)[0].equals(uuid) && !list.get(i)[2].equals("null")) {
                        final int finalI = i;
                        try {
                            loginThread.Send(setPackages.AttentionPg(2, 3, Integer.parseInt(list.get(finalI)[2])));
                            Thread.sleep(250);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
                for (int i = 0; i < adminList.size(); i++) {
                    if (adminList.get(i)[0].equals(uuid)) {
                        final int finalI = i;
                        loginThread.Send(setPackages.AttentionPg(2, 3, Integer.parseInt(adminList.get(finalI)[2])));
                    }
                }
            }
        }).start();

    }

    /**
     * @param varid 节点变量ID
     * @User SunLight
     * @Data 2018/1/11 16:00
     * @Method 取消变量关注方法
     **/
    public void CancleVarByVarId(final String varid) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < list.size(); i++) {
                    if (list.get(i)[5].equals(varid) && !list.get(i)[2].equals("null")) {
                        final int finalI = i;
                        loginThread.Send(setPackages.AttentionPg(2, 3, Integer.parseInt(list.get(finalI)[2])));
                    }
                }
                for (int i = 0; i < adminList.size(); i++) {
                    if (adminList.get(i)[0].equals(varid)) {
                        final int finalI = i;
                        loginThread.Send(setPackages.AttentionPg(2, 3, Integer.parseInt(adminList.get(finalI)[2])));
                    }
                }
            }
        }).start();


    }

    /**
     * @param uuid 节点设备ID
     * @User SunLight
     * @Data 2018/1/11 16:01
     * @Method 关注设备方法
     **/
    public void AttenDevByUiid(final String uuid) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < list.size(); i++) {
                    if (list.get(i)[0].equals(uuid) && !list.get(i)[2].equals("null")) {
                        final int finalI = i;
                        try {
                            loginThread.Send(setPackages.AttentionPg(1, 2, Integer.parseInt(list.get(finalI)[3])));
                            Thread.sleep(250);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }).start();
    }

    /**
     * @param uuid 节点设备ID
     * @User SunLight
     * @Data 2018/1/11 16:01
     * @Method 取消设备关注方法
     **/
    public void CancleDevByUiid(final String uuid) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < list.size(); i++) {
                    if (list.get(i)[0].equals(uuid)) {
                        final int finalI = i;
                        try {
                            loginThread.Send(setPackages.AttentionPg(2, 2, Integer.parseInt(list.get(finalI)[3])));
                            Thread.sleep(200);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }).start();
    }

    /**
     * @User SunLight
     * @Data 2018/1/11 16:02
     * @Method 取消所有关注方法
     **/
    public void CancleAllAtten() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                loginThread.Send(setPackages.AttentionPg(2, 1, 0));
            }
        }).start();
    }

    /**
     * @param uiid  节点设备ID
     * @param index 设备ID索引
     * @param var   设置值
     * @User SunLight
     * @Data 2018/1/11 16:02
     * @Method 设置变量方法（通过UIid）
     **/
    public void setVariableByUiid(final String uiid, final String index, final int var) {
        if (FindByUiid(uiid, index) != -1) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    loginThread.Send(setPackages.VariablePg(FindByUiid(uiid, index), var));
                }
            }).start();
        }
    }

    /**
     * @param uiid  节点设备ID
     * @param index 设备ID索引
     * @User SunLight
     * @Data 2018/1/11 16:03
     * @Method 获取变量值方法
     **/
    public String getVariableByUiid(final String uiid, final String index) {
        if (FindByUiid(uiid, index) != -1) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    loginThread.Send(setPackages.VariablePg(FindByUiid(uiid, index)));
                }
            }).start();
        }
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return setPackages.getData().Number;
    }

    /**
     * @param varId 节点变量ID
     * @User SunLight
     * @Data 2018/1/11 16:03
     * @Method 获取变量值方法
     **/
    public String getVariableByVarId(final String varId) {
        if (FindByVarId(varId) != -1) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    loginThread.Send(setPackages.VariablePg(FindByVarId(varId)));
                }
            }).start();
        }
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return setPackages.getData().Number;
    }

    /**
     * @param varId 节点变量ID
     * @param var   设置值
     * @User SunLight
     * @Data 2018/1/11 16:03
     * @Method 设置变量方法（通过VarID）
     **/
    public void setVariableByVarId(final String varId, final int var) {
        if (FindByVarId(varId) != -1) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    loginThread.Send(setPackages.VariablePg(FindByVarId(varId), var));
                }
            }).start();
        }
    }

    /**
     * @param uuid 节点设备ID
     * @param key  设置键值
     * @User SunLight
     * @Data 2018/1/11 16:04
     * @Method 设置键值方法（通过UIID）
     **/
    public void setKeyByUiid(final String uuid, final int key) {
        if (FindByDevId(uuid) != -1) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    loginThread.Send(setPackages.KeyPg(FindByDevId(uuid), key));

                }
            }).start();
        }
    }


    /**********************************************************************************************************
     * 工具方法:
     * FindByVarId(String varId)根据varId 查找对应的变量ID在List中的位置
     *
     * FindByUiid(String uiid, String index)根据Uiid 和变量索引index查找对应的变量ID在List中的位置
     *
     * FindByDevId(String varId)根据varId 查找对应的设备ID在List中的位置
     *
     * FindByDevId(String uiid, String index) 根据Uiid 和变量索引查index找对应的设备ID在List中的位置
     *
     * writeId(String AppId)   将字符串转化为字节数组
     * ********************************************************************************************************/
    private int FindByVarId(String varId) {
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i)[5].equals(varId)) {
                return Integer.parseInt(list.get(i)[2]);
            }
        }
        return -1;
    }

    private int FindByUiid(String uiid, String index) {
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i)[0].equals(uiid) && list.get(i)[1].equals(index) && !list.get(i)[2].equals("null")) {
                return Integer.parseInt(list.get(i)[2]);
            }
        }
        return -1;
    }

    private int FindByDevId(String uiid) {
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i)[0].equals(uiid)) {
                return Integer.parseInt(list.get(i)[3]);
            }
        }
        return -1;
    }

}
