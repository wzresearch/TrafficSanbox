package com.example.wzsdk.login;

import com.example.wzsdk.attention.AttenDispose;
import com.example.wzsdk.callback.LoginStateCallback;
import com.example.wzsdk.callback.ReceiveData;
import com.example.wzsdk.commpg.SetPackages;
import com.example.wzsdk.constant.PackgeExplain;
import com.example.wzsdk.tools.ToolsClass;

/**
 * Created by Admintor on 2018/1/10.
 */

public class ReciverData implements ReceiveData {
    private SendUtils sendUtils;
    private SetPackages setPackages;
    private SendData sendData;
    private AttenDispose attenDispose;
    private LoginStateCallback loginState;


    public ReciverData(SendData sendData, SendUtils sendUtils, LoginStateCallback loginState) {
        this.sendData = sendData;
        this.sendUtils = sendUtils;
        this.loginState = loginState;
        setPackages = sendUtils.getSetPackages();
        attenDispose = new AttenDispose(sendUtils);
    }

    /**
     * @User SunLight
     * @Data 2018/1/11 16:23
     * @Method 数据接收回调(数据处理)
     **/
    @Override
    public void receive(byte[] number) {
        switch (number[6]) {
            case PackgeExplain.PT_SRV_LOGIN:
                if (number[5] == 1) {
                    System.arraycopy(number, 20, setPackages.getData().SnHash, 0, 4);
                    sendData.sendData(number);

                } else {
                    System.arraycopy(number, 28, setPackages.getData().SecretKey, 0, 2);
                    sendData.sendData(number);

                }
                break;

            case PackgeExplain.PT_BULK_OPERATE:
                switch (number[5]) {
                    //获取主机下各种id长度
                    case 3:
                        System.arraycopy(number, 58, setPackages.getData().BulkNum, 0, 2);
                        System.arraycopy(number, 34, setPackages.getData().Length, 0, 2);
                        sendData.sendData(number);

                        break;
                    //保存设备id
                    case 4:
                        for (int i = 0; i < (number.length - 28) / 132; i++) {
                            setPackages.getData().Id.add(new byte[]{number[24 + 132 * i], number[25 + 132 * i]});
                            setPackages.getData().State.add(number[66 + 132 * i]);
                            setPackages.getData().Id_Str.add(ToolsClass.BytesToInteger(new byte[]{number[24 + 132 * i], number[25 + 132 * i]}, 10));
                            setPackages.getData().FileID.add(new byte[]{number[52 + 132 * i], number[53 + 132 * i], number[54 + 132 * i], number[55 + 132 * i]});
                        }
                        break;
                    case 10:
                        for (int i = 0; i < (number.length - 28) / 132; i++) {
                            setPackages.getData().Id.add(new byte[]{number[24 + 132 * i], number[25 + 132 * i]});
                            setPackages.getData().State.add(number[66 + 132 * i]);
                            setPackages.getData().Id_Str.add(ToolsClass.BytesToInteger(new byte[]{number[24 + 132 * i], number[25 + 132 * i]}, 10));
                            setPackages.getData().FileID.add(new byte[]{number[52 + 132 * i], number[53 + 132 * i], number[54 + 132 * i], number[55 + 132 * i]});
                        }
                        sendData.sendData(number);

                        break;
                    //保存变量id
                    case 5:
                        for (int i = 0; i < (number.length - 28) / 20; i++) {
                            setPackages.getData().Sid.add(new byte[]{number[24 + 20 * i], number[25 + 20 * i], number[26 + 20 * i], number[27 + 20 * i]});
                            setPackages.getData().MainDevID.add(new byte[]{number[40 + 20 * i], number[41 + 20 * i]});
                            setPackages.getData().Name.add(ToolsClass.BytesToInteger(new byte[]{number[40 + 20 * i], number[41 + 20 * i]}, 10));
                            setPackages.getData().VarIdx.add(number[42 + 20 * i]);
                        }
                        break;
                    case 11:
                        for (int i = 0; i < (number.length - 28) / 20; i++) {
                            setPackages.getData().Sid.add(new byte[]{number[24 + 20 * i], number[25 + 20 * i], number[26 + 20 * i], number[27 + 20 * i]});
                            setPackages.getData().MainDevID.add(new byte[]{number[40 + 20 * i], number[41 + 20 * i]});
                            setPackages.getData().Name.add(ToolsClass.BytesToInteger(new byte[]{number[40 + 20 * i], number[41 + 20 * i]}, 10));
                            setPackages.getData().VarIdx.add(number[42 + 20 * i]);
                        }
                        sendData.sendData(number);
                        break;
                }
                break;
            case PackgeExplain.PT_BULK_FOLLOW:
                attenDispose.disPose(number, setPackages.getData(), loginState);


            case PackgeExplain.PT_BULK_FOLLOWING:
                attenDispose.disPoseing(number, setPackages.getData(), loginState);
                break;

            case PackgeExplain.PT_BULK_KEY_SUCCESS:
                attenDispose.setPose(number, setPackages.getData(), loginState);
                break;
            case PackgeExplain.PT_BULK_SET_KEY:
                attenDispose.setPose(number, setPackages.getData(), loginState);
                break;
            case PackgeExplain.PT_BULK_KEY_BACK:
                attenDispose.disSetKeying(number, setPackages.getData(), loginState);
                break;
        }
    }


}
