package com.example.wzsdk.login;

import android.util.Log;

import com.example.wzsdk.callback.LoginStateCallback;
import com.example.wzsdk.callback.SendDataCallback;
import com.example.wzsdk.commpg.SetPackages;
import com.example.wzsdk.constant.ClientState;
import com.example.wzsdk.constant.Data;
import com.example.wzsdk.tools.ToolsClass;

/**
 * Created by Admintor on 2018/1/10.
 */

public class SendData implements SendDataCallback {
    private SendUtils sendUtils;
    private SetPackages setPackages;
    private LoginStateCallback loginState;
    private LoginThread loginThread;
    private HeartThread heartThread;

    static int lengh;//主机设备id总个数
    static int varLengh;//变量id总个数

    public SendData(SendUtils sendUtils, LoginStateCallback loginState, HeartThread heartThread) {
        this.sendUtils = sendUtils;
        this.loginState = loginState;
        this.heartThread = heartThread;
        setPackages = sendUtils.getSetPackages();
    }

    @Override
    public void sendData(byte[] number) {

        switch (number[6]) {
            case 8:
                if (number[5] == 1) {
                    loginThread.Send(setPackages.LoginPg());
                } else {
                    loginThread.Send(setPackages.BulkPg());
                }
                break;
            case 2:
                switch (number[5]) {
                    case 3:
                        /**
                         * 发送设备Bulack
                         * */
                        if (setPackages.getData().BulkNum[0] % 10 != 0) {
                            lengh = setPackages.getData().BulkNum[0] / 10 + 1;
                        } else {
                            lengh = setPackages.getData().BulkNum[0] / 10;
                        }

                        if (lengh == 0) {
                            sendDataSuccess(ClientState.Connected);
                        } else {
                            for (int i = 0; i < lengh; i++) {
                                if (setPackages.getData().BulkNum[0] % 10 != 0) {
                                    if (i == lengh - 1) {
                                        loginThread.Send(setPackages.BulkPg(4, (byte) (1 + (10 * i)), (byte) (setPackages.getData().BulkNum[0] % 10), (byte) 10));
                                    } else {
                                        loginThread.Send(setPackages.BulkPg(4, (byte) (1 + (10 * i)), (byte) 10, (byte) 4));
                                    }
                                } else {
                                    if (i == lengh - 1) {
                                        loginThread.Send(setPackages.BulkPg(4, (byte) (1 + (10 * i)), (byte) 10, (byte) 10));
                                    } else {
                                        loginThread.Send(setPackages.BulkPg(4, (byte) (1 + (10 * i)), (byte) 10, (byte) 4));
                                    }
                                }
                            }
                        }
                        break;
                    //变量Black
                    case 10:
                        /**
                         * 发送变量Bulck
                         * */
                        if (setPackages.getData().Length[0] % 70 != 0) {
                            varLengh = setPackages.getData().Length[0] / 70 + 1;
                        } else {
                            varLengh = setPackages.getData().Length[0] / 70;
                        }
                        for (int i = 0; i < varLengh; i++) {
                            if (setPackages.getData().Length[0] % 70 != 0) {
                                if (i == varLengh - 1) {
                                    loginThread.Send(setPackages.BulkPg(1, (byte) (1 + (70 * i)), (byte) (setPackages.getData().BulkNum[0] % 70), (byte) 11));
                                } else {
                                    loginThread.Send(setPackages.BulkPg(1, (byte) (1 + (70 * i)), (byte) 70, (byte) 5));
                                }
                            } else {
                                if (i == varLengh - 1) {
                                    loginThread.Send(setPackages.BulkPg(1, (byte) (1 + (70 * i)), (byte) 70, (byte) 11));
                                } else {
                                    loginThread.Send(setPackages.BulkPg(1, (byte) (1 + (70 * i)), (byte) 70, (byte) 5));
                                }
                            }
                        }
                        break;
                    case 11:
                        Corresponding(setPackages);
                        break;
                }
                break;
        }
    }

    @Override
    public void sendDataSuccess(int state) {
        switch (state) {
            case ClientState.Connected:
                Management(setPackages.getData());
                heartThread.start();
                loginState.LoginStatus(true, ToolsClass.Dut_ID(setPackages.getData().SnHash, 10));
                break;
            case ClientState.Connecting:
                loginState.LoginStatus(false, ToolsClass.Dut_ID(setPackages.getData().SnHash, 10));
                break;
        }

    }

    /**
     * @User SunLight
     * @Data 2018/1/11 16:24
     * @Method 初始化设备变量列表/系统变量列表
    **/
    public void Management(Data data) {
        if (data != null) {
            sendUtils.getList().clear();
            sendUtils.getAdminList().clear();
            sendUtils.getN_list().clear();
            sendUtils.getN_admin().clear();
            for (int i = 0; i < data.Node.size(); i++) {
                if (data.Node.get(i)[2].equals("0")) {
                    sendUtils.getAdminList().add(new String[]{data.Node.get(i)[0], data.Node.get(i)[2], data.Node.get(i)[3], data.Node.get(i)[5], data.Node.get(i)[4], "admin"});
                } else {
                    sendUtils.getList().add(new String[]{data.Node.get(i)[1], data.Node.get(i)[2], data.Node.get(i)[3], data.Node.get(i)[5], data.Node.get(i)[4], data.Node.get(i)[0]});
                }
            }
            for (int i = 0; i < data.NodeDev.size(); i++) {
                if (data.NodeDev.get(i)[2].equals("1")) {
                    sendUtils.getList().add(new String[]{data.NodeDev.get(i)[1], data.NodeDev.get(i)[2], data.NodeDev.get(i)[5], data.NodeDev.get(i)[3], data.NodeDev.get(i)[4], data.NodeDev.get(i)[0]});
                }
            }
            for (int i = 0; i < sendUtils.getList().size(); i++) {
                if (sendUtils.getList().get(i)[0].equals("系统变量")) {
                    sendUtils.getList().remove(i);
                }
            }

            for (int i = 0; i < sendUtils.getList().size(); i++) {
                sendUtils.getN_list().add(new String[]{sendUtils.getList().get(i)[0], sendUtils.getList().get(i)[1], sendUtils.getList().get(i)[4], sendUtils.getList().get(i)[5]});
            }
            for (int i = 0; i < sendUtils.getAdminList().size(); i++) {
                sendUtils.getN_admin().add(new String[]{sendUtils.getAdminList().get(i)[0], sendUtils.getAdminList().get(i)[1], sendUtils.getAdminList().get(i)[5]});
            }
        }
    }

    /**
     * @User SunLight
     * @Data 2018/1/11 16:24
     * @Method 添加修改
    **/
    public void Corresponding(SetPackages setPackages) {
        for (int i = 0; i < setPackages.getData().FileID.size(); i++) {
            setPackages.getData().NodeDev.add(new String[]{setPackages.getData().Id_Str.get(i), ToolsClass.BytesToInteger(setPackages.getData().FileID.get(i), 10), 1 + "", i + "", 1 + "" + "", "null"});
        }
        for (int i = 0; i < setPackages.getData().Name.size(); i++) {
            for (int j = 0; j < setPackages.getData().Id_Str.size(); j++) {
                if (setPackages.getData().Name.get(i).equals(setPackages.getData().Id_Str.get(j))) {
                    setPackages.getData().I.add(i);
                    setPackages.getData().Node.add(new String[]{ToolsClass.BytesToInteger(new byte[]{setPackages.getData().Sid.get(i)[0], setPackages.getData().Sid.get(i)[1]}, 10), ToolsClass.BytesToInteger(setPackages.getData().FileID.get(j), 10), setPackages.getData().VarIdx.get(i) + "", i + "", setPackages.getData().State.get(j) + "", j + ""});
                } else {
                    setPackages.getData().I.add(i);
                    if (j == 0)
                        setPackages.getData().Node.add(new String[]{ToolsClass.BytesToInteger(new byte[]{setPackages.getData().Sid.get(i)[0], setPackages.getData().Sid.get(i)[1]}, 10), "系统变量", setPackages.getData().VarIdx.get(i) + "", i + "", 1 + "", j + ""});
                }
            }
        }
        for (int j = 0; j < setPackages.getData().Node.size(); j++) {
            if (setPackages.getData().Node.get(j)[2].equals("1")) {
                setPackages.getData().J.add(Integer.parseInt(setPackages.getData().Node.get(j)[3]));
            } else if (setPackages.getData().Node.get(j)[2].equals("2") && setPackages.getData().Node.get(j)[1].equals("30008")) {
                setPackages.getData().J.add(Integer.parseInt(setPackages.getData().Node.get(j)[3]));
            } else if (setPackages.getData().Node.get(j)[2].equals("0")) {
                setPackages.getData().J.add(Integer.parseInt(setPackages.getData().Node.get(j)[3]));
            }
            for (int i = 0; i < setPackages.getData().NodeDev.size(); i++) {
                if (setPackages.getData().Node.get(j)[1].equals(setPackages.getData().NodeDev.get(i)[1])) {
                    setPackages.getData().NodeDev.remove(i);
                }
            }
        }

        if (setPackages.getData().J.size() != 0 || setPackages.getData().NodeDev.size() != 0) {
            sendDataSuccess(ClientState.Connected);
            setPackages.getData().isState = ClientState.Connected;
//            heartThread.start();
        } else {
            sendDataSuccess(ClientState.Connecting);
            setPackages.getData().isState = ClientState.Connecting;
        }
    }

    public void setLoginThread(LoginThread loginThread) {
        this.loginThread = loginThread;
    }
}
