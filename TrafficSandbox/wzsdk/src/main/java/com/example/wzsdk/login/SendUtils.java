package com.example.wzsdk.login;

import com.example.wzsdk.commpg.SetPackages;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Admintor on 2018/1/10.
 */

public class SendUtils {
    private SetPackages setPackages;
    private List<String[]> list;           //主机下节点设备变量列表
    private List<String[]> adminList;    //系统变量列表

    private List<String[]> n_list;
    private List<String[]> n_admin;

    public SendUtils() {
        setPackages = new SetPackages();
        list = new ArrayList<>();
        adminList = new ArrayList<>();

        n_list = new ArrayList<>();
        n_admin = new ArrayList<>();
    }

    public List<String[]> getN_list() {
        return n_list;
    }

    public void setN_list(List<String[]> n_list) {
        this.n_list = n_list;
    }

    public List<String[]> getN_admin() {
        return n_admin;
    }

    public void setN_admin(List<String[]> n_admin) {
        this.n_admin = n_admin;
    }

    public SetPackages getSetPackages() {
        return setPackages;
    }

    public void setSetPackages(SetPackages setPackages) {
        this.setPackages = setPackages;
    }

    public List<String[]> getList() {
        return list;
    }

    public void setList(List<String[]> list) {
        this.list = list;
    }

    public List<String[]> getAdminList() {
        return adminList;
    }

    public void setAdminList(List<String[]> adminList) {
        this.adminList = adminList;
    }
}
