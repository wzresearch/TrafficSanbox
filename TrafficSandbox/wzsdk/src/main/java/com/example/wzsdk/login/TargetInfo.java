package com.example.wzsdk.login;

import com.example.wzsdk.tools.StringValidationUtils;

/**
 * Created by Admintor on 2018/1/10.
 */

public class TargetInfo {
    private String ip;
    private int port;
    private String password;


    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }
}
