package com.example.wzsdk.scan;

import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Admintor on 2018/1/9.
 */

public class ScanSaveEigenValue {
    private Socket socket;
    private byte[] snHash = new byte[4];
    private byte[] hosttype = new byte[2];


    public Socket getSocket() {
        return socket;
    }

    public void setSocket(Socket socket) {
        this.socket = socket;
    }

    public byte[] getSnHash() {
        return snHash;
    }

    public void setSnHash(byte[] snHash) {
        this.snHash = snHash;
    }

    public byte[] getHosttype() {
        return hosttype;
    }

    public void setHosttype(byte[] hosttype) {
        this.hosttype = hosttype;
    }
}
