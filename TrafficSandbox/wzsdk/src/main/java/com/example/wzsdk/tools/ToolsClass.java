package com.example.wzsdk.tools;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;

import java.math.BigInteger;
import java.util.Arrays;

/**
 * Created by Admintor on 2018/1/9.
 */

public class ToolsClass {
    public static final int WZONE_PORT = 5800;
    public static final int SINGLE_BYTE_MASK = 0xFF;


    /**
     * @param saveId 需要保存到的数组
     * @param byId   appId字符串
     * @User SunLight
     * @Data 2018/1/11 16:28
     * @Method APPID转换（字符串转byte数组）
     **/
    public static void WriteToId(byte[] saveId, String byId) {
        System.arraycopy(byId.getBytes(), 0, saveId, 0, 4);
    }

    /**
     * @param bytes 数组型校验码
     * @User SunLight
     * @Data 2018/1/11 16:28
     * @Method 校验码转换
     **/
    public static short SecretKey(byte[] bytes) {
        byte[] newBytes = {bytes[1], bytes[0]};
        short key = (short) (((newBytes[0] & 0x00FF) << 8) | (0x00FF & newBytes[1]));
        return key;
    }

    /**
     * @User SunLight
     * @Data 2018/1/11 16:29
     * @Method 获取本地ip
     **/
    public static String getLocalHostIP(Context context) {
        @SuppressLint("WrongConstant") WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        if (!wifiManager.isWifiEnabled()) {
            wifiManager.setWifiEnabled(true);
        }
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        int ipAddress = wifiInfo.getIpAddress();

        return intToIp(ipAddress).substring(0, intToIp(ipAddress).lastIndexOf("."));
    }

    /**
     * @User SunLight
     * @Data 2018/1/11 16:29
     * @Method 格式化ip地址
     **/
    private static String intToIp(int i) {
        return (i & 0xFF) + "." + ((i >> 8) & 0xFF) + "." + ((i >> 16) & 0xFF) + "." + ((i >> 24) & 0xFF);
    }

    /**
     * @param bytes 主机Id数
     * @param radix 转换进制
     * @User SunLight
     * @Data 2018/1/11 16:29
     * @Method
     **/
    public static String Dut_ID(byte[] bytes, int radix) {
        byte[] nbytes = new byte[bytes.length];
        for (int i = 0; i < bytes.length; i++) {
            nbytes[i] = bytes[bytes.length - i - 1];
        }
        return new BigInteger(1, nbytes).toString(radix);
    }


    /**
     * @User SunLight
     * @Data 2018/1/11 16:34
     * @Method Hash码运算
     **/
    public static byte[] makeHash(byte[] data, int offSet, int length, short secretKey) {
        if (data == null || offSet + length > data.length)
            return null;
        int hashCode = makeHashInt(data, offSet, length);
        short reHashCode = (short) (hashCode & 0xFFFF);

        if (secretKey != 0) {
            reHashCode ^= secretKey;
        }
        byte[] hashBytes = new byte[2];
        hashBytes[0] = (byte) (reHashCode & SINGLE_BYTE_MASK);
        hashBytes[1] = (byte) ((reHashCode >> 8) & SINGLE_BYTE_MASK);

        return hashBytes;
    }


    public static int makeHashInt(byte[] data, int offSet, int length) {
        if (data == null || length > data.length)
            return -1;

        int hashCode = -1;

        for (int i = offSet, end = offSet + length; i < end; ++i) {
            hashCode += data[i] & SINGLE_BYTE_MASK;
            hashCode += hashCode << 5;
        }

        return hashCode;
    }

    /**
     * @param fistArrays   包头
     * @param secondArrays 包尾
     * @User SunLight
     * @Data 2018/1/11 16:34
     * @Method 将二维数组转为一维数组
     **/
    public static byte[] TwoArraysMerge(byte[][] fistArrays, byte[][] secondArrays) {
        int aLenght = 0;
        for (int i = 0; i < fistArrays.length; i++) {
            aLenght += fistArrays[i].length;
        }
        for (int i = 0; i < secondArrays.length; i++) {
            aLenght += secondArrays[i].length;
        }
        byte[] newBytes = new byte[aLenght];
        int f = 0, t = 0;
        for (int i = 0; i < fistArrays.length; i++) {
            for (int j = 0; j < fistArrays[i].length; j++) {
                newBytes[f] = fistArrays[i][j];
                f++;
            }
        }
        for (int i = 0; i < secondArrays.length; i++) {
            for (int j = 0; j < secondArrays[i].length; j++) {
                newBytes[f + t] = secondArrays[i][j];
                t++;
            }
        }
        return newBytes;
    }


    /**
     * @param length 数据包长度
     * @User SunLight
     * @Data 2018/1/11 16:35
     * @Method
     **/
    public static byte[] LenghtToBytes(int length) {
        byte[] newBytes = new byte[2];
        newBytes[0] = (byte) ((length ^ 0xA5A5) & 0xff);
        newBytes[1] = (byte) ((length ^ 0xa5a5) >> 8 & 0xff);
        return newBytes;
    }

    /**
     * @param wantBytes 需要转换的数组
     * @param radix     转换进制
     * @User SunLight
     * @Data 2018/1/11 16:51
     * @Method 字节数组转字符串
     **/
    public static String BytesToInteger(byte[] wantBytes, int radix) {
        switch (wantBytes.length) {
            case 2:
                byte[] tb = {wantBytes[1], wantBytes[0]};
                return new BigInteger(1, tb).toString(radix);
            case 4:
                byte[] tbs = {wantBytes[3], wantBytes[2], wantBytes[1], wantBytes[0]};
                return new BigInteger(1, tbs).toString(radix);
        }
        return null;
    }


    /**
     * @param fistArrays   包头的长度
     * @param secondArrays 包类的长度
     * @User SunLight
     * @Data 2018/1/11 16:50
     * @Method 二维数组长度求和
     **/
    public static byte TwoArraysLenght(byte[][] fistArrays, byte[][] secondArrays) {
        int fistLenght = 0;
        int secondLenght = 0;
        for (int i = 0; i < fistArrays.length; i++) {
            fistLenght += fistArrays[i].length;
        }
        for (int i = 0; i < secondArrays.length; i++) {
            secondLenght += secondArrays[i].length;
        }

        return (byte) (fistLenght + secondLenght);
    }

    /**
     * @param newArrays 复制到数组
     * @param oldArrays 被复制的数组
     * @User SunLight
     * @Data 2018/1/11 16:36
     * @Method 数组复制
     **/
    public static void ArraysCopy(byte[] newArrays, byte[] oldArrays) {
        for (int i = 0; i < oldArrays.length; i++) {
            newArrays[i] = oldArrays[i];
        }
    }


}
